# Fake News Detection and Classification

This repo tracks the materials for the NUS lifehack 2021 Workshop - Fake News Detection and Classification 

You would need two things for the workshop - **data** for analysis, and **an environment** for analytics. 

## Data
Please download the data set from either this repo ([fakenewskdd2020.zip](https://gitlab.com/nus-lifehack-datathon-2021/fakenews/-/blob/main/fakenewskdd2020.zip)) or from kaggle for the hands-on component. 

### Alternative - download from kaggle directly

Kaggle site -> https://www.kaggle.com/c/fakenewskdd2020/data

Use the Download All button, and you should get a file called fakenewskdd2020.zip. We will use this zip file for analysis.

- Note that you will need to have a kaggle account (or login through your preferred auth provider) to download the data

## Environment 

Google has great (free) resources for data analytics in the form of Google Colab. I'll be doing the workshop on google colab too. Alternatively, you can just use your own jupyter notebook platform (at least python 3.7). 

Google Colab -> head to https://research.google.com/colaboratory/ , and set up an account.

- Note that you will need a google account to start a notebook
- Upload the template notebook from this repo (to be released later) to get started!
- All the packages to install will be inside the notebook
- Note that if you are using your own platform, you will need to install pandas and numpy manually (`!pip install numpy and pandas`)
